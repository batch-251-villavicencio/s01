name = "John"
age = 42
work = "Influencer"
movie = "Iron Man"
rating = 98.5
print(f"I am {name}, and I am {age} years old, I work as an {work} and my rating for {movie} is {rating}%")
num1, num2, num3 = 56, 17, 95
print(num1*num2)
print(num1<num3)
print(num3+num2)